#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

typedef enum {False, True} Boolean;

typedef enum {Player1=1, Player2} Jogador;

typedef struct
{
    int x, y;
    Boolean movida;
}Peao;
typedef struct
{
    int x, y;
    Boolean movida;
}Torre;
typedef struct
{
    int x, y;
    Boolean movida;
}Rei;

typedef struct
{
    Rei rei;
    Torre torrer;
    Peao peao[8];
    unsigned short int t, c, b, q, k, p;
}Pecas;

char mapaatual[41][78];
char mapatabuleiro[41][78]=
{
    "00000000000000000000000000000000000000000000000000000000000000000000000000000",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x   T   x   C   x   B   x   D   x   R   x   B   x   C   x   T   x 0   1   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x   P   x   P   x   P   x   P   x   P   x   P   x   P   x   P   x 0   2   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x       x       x       x       x       x       x       x       x 0   3   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x       x       x       x       x       x       x       x       x 0   4   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x       x       x       x       x       x       x       x       x 0   5   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x       x       x       x       x       x       x       x       x 0   6   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x   p   x   p   x   p   x   p   x   p   x   p   x   p   x   p   x 0   7   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 x   t   x   c   x   b   x   d   x   r   x   b   x   c   x   t   x 0   8   0",
    "0 x       x       x       x       x       x       x       x       x 0       0",
    "0 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 0-------0",
    "000000000000000000000000000000000000000000000000000000000000000000000.......0",
    "0 |       |       |       |       |       |       |       |       | 0.......0",
    "0 |   a   |   b   |   c   |   d   |   e   |   f   |   g   |   h   | 0.......0",
    "0 |       |       |       |       |       |       |       |       | 0.......0",
    "00000000000000000000000000000000000000000000000000000000000000000000000000000",
    "|Jogador 1: X |REI:   |DAMAS:   |TORRES:   |CAVALOS:   |BISPOS:   |PEOES:   |",
    "|Jogador 2:   |rei:   |damas:   |torres:   |cavalos:   |bispos:   |peoes:   |"
};

int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;
    
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
    
    ch = getchar();
    
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);
    
    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }
    
    return 0;
}

void gotoxy(int x,int y){
    printf("%c[%d;%df",0x1B,y,x);
}

void exibirMapa(){
    int i;
    for(i=0;i<41;i++){
        printf("%s\n", mapaatual[i]);
    }
}
void atualizarmapa(){
    int i;
    for(i=0;i<41;i++)
        strcpy(mapaatual[i],mapatabuleiro[i]);
    
}
char esperatecla(){
    while(1)
        if(kbhit())
            return getchar();
}
void direcao(char const c, int *x, int *y){
    if(c == 'A' || c == 'a'){
        if(!(*x-8<7))
            *x-=8;
    }
    else if(c == 's' || c == 'S'){
        if(!(*y+4>(4*8)))
            *y+=4;
    }
    else if(c == 'd' || c == 'D'){
        if(!(*x+8>(8*8)))
            *x+=8;
    }
    else if(c == 'w' || c == 'W'){
        if(!(*y-4<4))
            *y-=4;
    }
    else if(c == 'q' || c == 'Q'){
        if(!(*y-4<4) && !(*x-8<7)){
            *y-=4;
            *x-=8;
        }
    }
    else if(c == 'e' || c == 'E'){
        if(!(*y-4<4) && !(*x+8>(8*8))){
            *y-=4;
            *x+=8;
        }
    }
    else if(c == 'z' || c == 'Z'){
        if(!(*y+4>(4*8)) && !(*x-8<7)){
            *y+=4;
            *x-=8;
        }
    }
    else if(c == 'x' || c == 'X'){
        if(!(*y+4>(4*8)) && !(*x+8>(8*8))){
            *y+=4;
            *x+=8;
        }
    }
}
Boolean pecaJogador(Jogador jogador, char c){
    if(jogador == Player1){
        if (!(c=='T' || c=='C' || c=='B' || c=='D' || c=='R' || c=='P'))
            return False;
    }
    else if(jogador == Player2){
        if (!(c=='t' || c=='c' || c=='b' || c=='d' || c=='r' || c=='p'))
            return False;
    }
    return True;
}
Boolean ValidaMovimento(Jogador jogador, char c, int xInicial, int yInicial, int xFinal, int yFinal, Pecas *pecas){

    int i;

    if(xFinal == xInicial && yInicial == yFinal)
        return False;

    if(c == 'p' || c == 'P'){
        if(xInicial != xFinal){
            if(yFinal > yInicial+4 || yFinal < yInicial-4 || yFinal == yInicial)
                return False;
            if (yInicial+4 == yFinal || yInicial-4 == yFinal){
                if(!(xInicial+8 == xFinal || xInicial-8 == xFinal)){
                    return False;
                }
                else{
                    if(mapaatual[yFinal-1][xFinal-1] == ' '){
                        return False;
                    }
                    else {
                        return !pecaJogador(jogador, mapaatual[yFinal-1][xFinal-1]);
                    }
                }
            }
        }
        else if(yFinal == yInicial+8 || yFinal == yInicial-8){
            for(i=0;i<8;i++){
                if(pecas->peao[i].x == xInicial && pecas->peao[i].y == yInicial){
                    if(pecas->peao[i].movida)
                        return False;
                    else {
                        if(mapaatual[yFinal-1][xFinal-1] != ' '){
                                return False;
                        }
                        pecas->peao[i].movida = True;
                        return True;
                    }
                }
            }
            return False;
        }
        else if(yFinal == yInicial+4 || yFinal == yInicial-4){
            if(mapaatual[yFinal-1][xFinal-1] != ' '){
                    return False;
            }
        }
        else 
            return False;
    }
    else if (c == 'T' || c == 't'){
        if(xFinal != xInicial && yInicial != yFinal)
            return False;
        if(xFinal != xInicial){
            if(xFinal > xInicial){
                for(i=1; (xInicial+(8*i)) < xFinal; i++)
                    if(mapaatual[yFinal-1][(xInicial+(8*i))-1] != ' ')
                        return False;
            }
            else if (xFinal < xInicial){
                for(i=1; (xInicial-(8*i)) > xFinal; i++)
                    if(mapaatual[yFinal-1][(xInicial-(8*i))-1] != ' ')
                        return False;
            }
        }
        if(yFinal != yInicial){
            if(yFinal > yInicial){
                for(i=1; (yInicial+(4*i)) < yFinal; i++)
                    if(mapaatual[yInicial+(4*i)-1][xFinal-1] != ' ')
                        return False;
            }
            else if (yFinal < yInicial) {
                for(i=1; (yInicial-(4*i)) > yFinal; i++)
                    if(mapaatual[yInicial-(4*i)-1][xFinal-1] != ' ')
                        return False;
            }
        }
        return !pecaJogador(jogador, mapaatual[yFinal-1][xFinal-1]);
    }

    return True;
}
Boolean validaJogada(Jogador jogador, char cJogador, int xInicial, int yInicial, int xFinal, int yFinal, Pecas *pecas){
    
    if(!pecaJogador(jogador, cJogador)){
        gotoxy(1,42);
        printf("Essa peca nao te pertence\n");
        usleep(1000000);
        return False;
    }

    if(!ValidaMovimento(jogador, cJogador,xInicial,yInicial,xFinal,yFinal, pecas)){
        gotoxy(1,42);
        printf("Movimento Invalido\n");
        usleep(1000000);
        return False;
    }

    return True;
}
void InicializaPecas(Pecas *Jogador1, Pecas *Jogador2){
    int i;

    Jogador1->t = 2;
    Jogador1->c = 2;
    Jogador1->b = 2;
    Jogador1->q = 1;
    Jogador1->k = 1;
    Jogador1->p = 8;
    Jogador1->rei.movida = False;
    Jogador1->torrer.movida = False;
    for(i=0;i<8;i++){
    	Jogador1->peao[i].movida = False;
    	Jogador1->peao[i].y = 8;
    	Jogador1->peao[i].x = 7 + (8*i);
    }
    
    Jogador2->t = 2;
    Jogador2->c = 2;
    Jogador2->b = 2;
    Jogador2->q = 1;
    Jogador2->k = 1;
    Jogador2->p = 8;
    Jogador2->rei.movida = False;
    Jogador2->torrer.movida = False;
    for(i=0;i<8;i++){
    	Jogador2->peao[i].movida = False;
    	Jogador2->peao[i].y = 28;
    	Jogador2->peao[i].x = 7 + (8*i);
    }
    
}
void AtualizaStatusPecas(Pecas Jogador1, Pecas Jogador2){
    char str[2];
    sprintf(str,"%d",Jogador1.k);
    mapaatual[39][20] = str[0];
    sprintf(str,"%d",Jogador1.q);
    mapaatual[39][30] = str[0];
    sprintf(str,"%d",Jogador1.t);
    mapaatual[39][41] = str[0];
    sprintf(str,"%d",Jogador1.c);
    mapaatual[39][53] = str[0];
    sprintf(str,"%d",Jogador1.b);
    mapaatual[39][64] = str[0];
    sprintf(str,"%d",Jogador1.p);
    mapaatual[39][74] = str[0];
    
    
    sprintf(str,"%d",Jogador2.k);
    mapaatual[40][20] = str[0];
    sprintf(str,"%d",Jogador2.q);
    mapaatual[40][30] = str[0];
    sprintf(str,"%d",Jogador2.t);
    mapaatual[40][41] = str[0];
    sprintf(str,"%d",Jogador2.c);
    mapaatual[40][53] = str[0];
    sprintf(str,"%d",Jogador2.b);
    mapaatual[40][64] = str[0];
    sprintf(str,"%d",Jogador2.p);
    mapaatual[40][74] = str[0];
}
int main(int argc, char const *argv[])
{
    char c, cAux;
    int i,x=7,y=4, xAux, yAux;
    Jogador jogador = Player1;
    Boolean Jogou = False;
    
    Pecas PecasJ1, PecasJ2;
    
    InicializaPecas(&PecasJ1, &PecasJ2);
    
    atualizarmapa();
    
    do{
        xAux = x;
        yAux = y;
        
        AtualizaStatusPecas(PecasJ1, PecasJ2);
        system("clear");
        gotoxy(1,1);
        exibirMapa();
        gotoxy(x,y);
        c = esperatecla();
        direcao(c,&x,&y);
        
        if (c == ' '){
            
            cAux = mapaatual[y-1][x-1];
            xAux = x;
            yAux = y;
            if(cAux != ' '){
                mapaatual[y-1][x-1] = ' ';
                do{
                    
                    system("clear");
                    gotoxy(1,1);
                    exibirMapa();
                    gotoxy(x,y);
                    c = esperatecla();
                    direcao(c,&x,&y);
                    if (c==' '){
                        //REGRAS ENTRAM AQUI
                        if(validaJogada(jogador, cAux, xAux, yAux, x, y, jogador == Player1 ? &PecasJ1 : &PecasJ2)){
                            mapaatual[y-1][x-1] = cAux;
                            Jogou = True;
                        }
                        else{
                            mapaatual[yAux-1][xAux-1] = cAux;
                            Jogou = False;
                        }
                    }
                }while(c!=' ');
            }
            if(Jogou){
                if(jogador == Player1){
                    jogador = Player2;
                    mapaatual[39][12] = ' ';
                    mapaatual[40][12] = 'X';
                }
                else if(jogador == Player2){
                    jogador = Player1;
                    mapaatual[39][12] = 'X';
                    mapaatual[40][12] = ' ';
                }
                
            }
            Jogou = False;
        }
        
        
    }while(c != 'p');
    
    system("clear");
    gotoxy(0,0);
    
    return 0;
}
